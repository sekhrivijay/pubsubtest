package com.micro.config;

import com.micro.PubSubApplication;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gcp.pubsub.PubSubAdmin;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class WebController {

    @Autowired
    private PubSubTemplate pubSubTemplate;

    @Autowired
    private PubSubAdmin pubSubAdmin;

    private static Log LOGGER = LogFactory.getLog(PubSubApplication.class);

    @PostMapping("/createTopic")
    public RedirectView createTopic(@RequestParam("topicName") String topicName) {
        this.pubSubAdmin.createTopic(topicName);

        return new RedirectView("/");
    }

    @PostMapping("/createSubscription")
    public RedirectView createSubscription(@RequestParam("topicName") String topicName,
                                           @RequestParam("subscriptionName") String subscriptionName) {
        this.pubSubAdmin.createSubscription(subscriptionName, topicName);

        return new RedirectView("/");
    }

    @GetMapping("/postMessage")
    public RedirectView publish(@RequestParam("message") String message,
                                @RequestParam("topicName") String topicName) {
        this.pubSubTemplate.publish(topicName, message, null);

        return new RedirectView("/");
    }

    @GetMapping("/subscribe")
    public RedirectView subscribe(@RequestParam("subscription") String subscriptionName) {
        this.pubSubTemplate.subscribe(subscriptionName, (pubsubMessage, ackReplyConsumer) -> {
            LOGGER.info("Message received from " + subscriptionName + " subscription. "
                    + pubsubMessage.getData().toStringUtf8());
            ackReplyConsumer.ack();
        });

        return new RedirectView("/");
    }

    @PostMapping("/deleteTopic")
    public RedirectView deleteTopic(@RequestParam("topic") String topicName) {
        this.pubSubAdmin.deleteTopic(topicName);

        return new RedirectView("/");
    }

    @PostMapping("/deleteSubscription")
    public RedirectView deleteSubscription(@RequestParam("subscription") String subscriptionName) {
        this.pubSubAdmin.deleteSubscription(subscriptionName);

        return new RedirectView("/");
    }
}
